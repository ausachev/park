#encoding: utf-8
'''
Created on 22.02.2013

@author: Администратор
'''

from google.appengine.ext import ndb

class PKspot(ndb.Model):
	total = ndb.IntegerProperty()
	free = ndb.IntegerProperty()
	polygon = ndb.GeoPtProperty()
	address = ndb.StringProperty()
	
	def to_dict(self):
		d = super(PKspot, self).to_dict()
		""" Pre-process datetime into strings as it is not JSON-serializable """
		for item in d.items():
			if item[0] == "polygon" and item[1]:
				d[item[0]] = [self.polygon.lat, self.polygon.lon]
				continue
		d['id'] = str(self.key.urlsafe())
		return d
