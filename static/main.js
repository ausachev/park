function App() {
	var self = this;
	
	self.total = ko.observable(0);
	self.free = ko.observable(0);
	self.address = ko.observable("Empty");
	self.long = 0;
	self.lat=0;
	
	self.map = L.map('map', {drawControl : true	}).setView([ 50.0, 36.25 ], 12);

	L.tileLayer('http://{s}.tile.cloudmade.com/77c132cb60f9451b8cdca8aed5c70ae3/82180/256/{z}/{x}/{y}.png',
					{	attribution : '<a href="#" id="attribution">Attribution</a>',
						maxZoom : 18
					}).addTo(self.map);

	self.map.on('draw:created', function(e) {
		var type = e.layerType, layer = e.layer;

		if (type === 'marker') {
			window.lat = layer.getLatLng().lat;
			window.long = layer.getLatLng().lng;
		}
		
		// Do whatever else you need to. (save to db, add to map etc)
		self.map.addLayer(layer);
	});

	self.currentMarker = null;
	self.addMode = false;
	
	$.getJSON('/api/pkspots', function(spots) {
		$.map(spots, function(pkspot) {
			var latlng = new L.LatLng(pkspot.polygon[0], pkspot.polygon[1]);
			var marker = L.marker(latlng);
			marker.bindPopup("<p>" + pkspot.address + "</p>" + "<p>"
					+ pkspot.free + "/" + pkspot.total + "</p>")
			marker.addTo(self.map);
		});
	});

	self.createParking = function() {
		$.post('/api/pkspots', {
			total : self.total,
			free : self.free,
			latitude : self.lat,
			longitude : self.long,
			address : self.address
		});
	};

}
var app = new App();
ko.applyBindings(app);